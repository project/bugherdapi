
    ____              __                  __   ___    ____  ____
   / __ )__  ______ _/ /_  ___  _________/ /  /   |  / __ \/  _/
  / __  / / / / __ `/ __ \/ _ \/ ___/ __  /  / /| | / /_/ // /  
 / /_/ / /_/ / /_/ / / / /  __/ /  / /_/ /  / ___ |/ ____// /   
/_____/\__,_/\__, /_/ /_/\___/_/   \__,_/  /_/  |_/_/   /___/   
            /____/                                              

CHANGELOG

Progress is impossible without change,
and those who cannot change their minds cannot change anything.
