
    ____              __                  __   ___    ____  ____
   / __ )__  ______ _/ /_  ___  _________/ /  /   |  / __ \/  _/
  / __  / / / / __ `/ __ \/ _ \/ ___/ __  /  / /| | / /_/ // /  
 / /_/ / /_/ / /_/ / / / /  __/ /  / /_/ /  / ___ |/ ____// /   
/_____/\__,_/\__, /_/ /_/\___/_/   \__,_/  /_/  |_/_/   /___/   
            /____/                                              


README

Drupal integration for BugHerd.

BugHerd turns client feedback into actionable tasks.

Your clients report issues by making annotations right from the site being worked on.

BugHerd turns these into full bug reports with all the info you need to fix the problem.

For more info visit the official BugHerd website on www.bugherd.com

-- GETTING STARTED --

1. Install Bugherd API in the usual way
   (https://www.drupal.org/node/1897420)
3. Go to Administration > Configuration > System > BugHerd
   (/admin/config/system/bugherd)
4. Enter the BugHerd project key found on Bugherd.com for the project under the install Bugherd option
5. Optionally disable BugHerd for admin pages
6. Click "Save configuration"

Bring on the bugs!

-- MAINTAINERS --

Fons Vandamme - https://www.drupal.org/u/fons-vandamme
